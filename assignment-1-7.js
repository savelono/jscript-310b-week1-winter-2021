document.write("<h2> * FIND THE MIDDLE DATE</h2>") ;

document.write("<p>On your own find the middle date(and time) between the following two dates:<br> \
1/1/2020 00:00:00 and 4/1/2020 00:00:00<br><br> Look online for documentation on Date objects.<br> \
Starting hint:<br>const endDate = new Date(2019, 3, 1); </p>") ;


const endDate = new Date(2019, 3, 1);

var date1 = new Date(2020,0,1)
var date2 = new Date(2020,3,1) 

var midpoint = new Date((date1.getTime() + date2.getTime()) / 2);

document.write(`<p>Date #1: ${date1}</p>`)
document.write(`<p>Date #2: ${date2}</p>`)
document.write(`<p>Mid Point: ${midpoint}</p>`)