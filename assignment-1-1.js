document.write("<p>MATH</p>") ;

document.write("<p>1. Pagliacci charges $16.99 for a 13” pizza and $19.99 for a 17” pizza. \
What is the area for each of these pizzas? \
(radius would be the listed size - i.e. 13\" - divided by 2)</p>") ;

let pizza_large_area = Math.PI * (17/2 * 17/2) ;
let pizza_small_area = Math.PI * (13/2 * 13/2) ;

document.write(`<p>Area of a Large Pagliacci Pizza: ${pizza_large_area} square inches</p>`) ;
document.write(`<p>Area of a Small Pagliacci Pizza: ${pizza_small_area} sqaure inches</p>`) ;

document.write("<br><br")

