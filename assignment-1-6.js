document.write("<h2>ADDRESS LINE</h2>") ;

document.write("<p>6. You are given a string in this format: \
firstName lastName(assume no spaces in either) streetAddress \
city, state zip(could be spaces in city and state)</p>") ;

var address = {
    firstName: "Matt", 
    lastName: "Birkland",
    streetAddress: "1620 Laffy Taffy Lane apt. #A", 
    city: "Seattle", 
    state: "WA",
    zipCode: "98124"
} ;

document.write(`<p>${address.firstName} ${address.lastName}<br>${address.streetAddress}\
<br>${address.city}, ${address.state} ${address.zipCode}`) ;