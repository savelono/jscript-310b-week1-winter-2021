document.write("<p>2. What is the cost per square inch of each pizza?</p>") ;

let pizza_large_area = Math.PI * (17/2 * 17/2) ;
let pizza_small_area = Math.PI * (13/2 * 13/2) ;

let pizza_large_cost = 19.99 ;
let pizza_small_cost = 16.99 ;

large_pizza_area_cost = pizza_large_cost/pizza_large_area ;
small_pizza_area_cost = pizza_small_cost/pizza_small_area ;

document.write(`<p>Large Pizza cost by square inch: ${large_pizza_area_cost}</p>`) ;
document.write(`<p>Small Pizza cost by square inch: ${small_pizza_area_cost}</p>`) ;

