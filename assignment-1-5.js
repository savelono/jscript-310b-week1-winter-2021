document.write("<h2>ADDRESS LINE</h2>") ;

document.write("<p>5. Create variables for firstName, lastName, \
streetAddress, city, state, and zip code. Use this information \
to create a formatted address block that could be printed onto an envelope.</p>") ;

var address = {
    firstName: "Matt", 
    lastName: "Birkland",
    streetAddress: "1620 Laffy Taffy Lane apt. #A", 
    city: "Seattle", 
    state: "WA",
    zipCode: "98124"
} ;

document.write(`<p>${address.firstName} ${address.lastName}<br>${address.streetAddress}\
<br>${address.city}, ${address.state} ${address.zipCode}`) ;

